﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication40
{
    static class LevenshteinDistance
    {
    
        public static string Compute(string s)
        {
            int n = s.Length;
            string path = @"C:\Users\a_kurmasheva\Desktop\words.txt";
            using (StreamReader sr = new StreamReader(path))
            {
                string t;
                while ((t = sr.ReadLine()) != null)
                {
                    int m = t.Length;
                    int[,] d = new int[n + 1, m + 1];
                    for (int i = 0; i <= n; d[i, 0] = i++)
                    {

                    }
                    for (int j = 0; j <= m; d[0, j] = j++)
                    {

                    }
                    for (int i = 1; i <= n; i++)
                    {
                        for (int j = 1; j <= m; j++)
                        {
                            int cost = (t[j - 1] == s[j - 1]) ? 0 : 1;
                            d[i, j] = Math.Min(Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1), d[i - 1, j - 1] + cost);
                        }
                    }



                    if (d[m, n] < 5)
                    {
                        return t;

                    }
                    return s;
                }
            }
        }    
    }
        class Program
        {
            static void Main(string[] args)
            {
                Console.WriteLine(LevenshteinDistance.Compute("10th"));
                Console.ReadKey();
            }
        }
    
     
}
