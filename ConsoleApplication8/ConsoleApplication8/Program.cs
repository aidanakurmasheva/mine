﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace ConsoleApplication8
{   

    class Program
    {
        static bool IsPhone(string s)
        {
            return Regex.IsMatch(s, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }   
        static void Main(string[] args)
        {
            string k = "(555)5551234";
            if (IsPhone(k) == true)
            {
                Console.WriteLine("match");
            }
            else
                Console.WriteLine("does not match");
            Console.ReadKey();
        }
    }
}
